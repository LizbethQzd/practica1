//import { ListaNoticias } from "@/componentes/noticias/listaNoticias";
'use client';
//import { createContext } from 'react';

//const MyContext = createContext();
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { inicio_sesion } from '@/hooks/Autenticacion';
import { estaSesion } from '@/hooks/SessionUtils';
import mensajes from '@/componentes/Mensajes';
//import { useRouter } from 'next/router';
import { useRouter } from 'next/navigation';


export default function Inicio() {
    //-----------------------------------------
    const router = useRouter();
    //-----------------------------------------
    const validationSchema = Yup.object().shape({
        cedula: Yup.string().required('Ingrese un correo'),
        clave: Yup.string().required('Ingrese su clave')
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const sendData = (data) => {
        //console.log(data);
        var data = { "identificador": data.cedula, "clave": data.clave, "funcion": "sesion" };
        /*enviar('', data).then((info) => {
            console.log(data);
        });*/
        inicio_sesion(data).then((info) => {
            console.log(info);
            if (!estaSesion()) {
                mensajes("Error en inicio de sesion", info.msg, "error");
            } else {
                mensajes("Has ingresado al sistema!", "Bienvenido usuario");
                router.push('/principal');
            }

        });
    };
    const blackBorderStyle = {
        border: '4px solid pink',
        padding: '60px',
        borderRadius: '20px'
    };

    return (
        <div className="container">
            <section className="vh-100">
                <div className="container py-5 h-100">
                    <div className="row d-flex align-items-center justify-content-center h-100">
                        <div className="col-md-8 col-lg-7 col-xl-6">
                            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg"
                                className="img-fluid" alt="Phone image" />
                        </div>
                        <div className="col-md-7 col-lg-5 col-xl-5 offset-xl-1">
                            <form onSubmit={handleSubmit(sendData)}>
                                <div className="mb-4" style={blackBorderStyle}>
                                    <div className="form-outline mb-4">
                                        <label className="form-label"><strong>Correo electrónico</strong></label>
                                        <input {...register('cedula')} type='text' name="cedula" id="cedula" className={`form-control ${errors.cedula ? 'is-invalid' : ''}`} />
                                        <div className='alert alert-danger invalid-feedback'>{errors.cedula?.message}</div>
                                    </div>

                                    <div className="form-outline mb-4">
                                        <label className="form-label"><strong>Clave</strong></label>
                                        <input {...register('clave')} type='password' name="clave" id="clave" className={`form-control ${errors.clave ? 'is-invalid' : ''}`} />
                                        <div className='alert alert-danger invalid-feedback'>{errors.clave?.message}</div>
                                    </div>

                                    <div className="d-grid gap-1">
                                        <button type="submit" className="btn btn-primary btn-block mb-4">
                                            <strong>Acceder</strong>
                                        </button>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    );



}
