'use client';
import { useEffect, useState } from "react";
import { obtenerDocumento } from "@/hooks/Conexion"; // Asumiendo que esta importación es correcta y está apuntando al archivo correcto
import { getToken } from "@/hooks/SessionUtils";
import Link from "next/link";

export default function Page() {
    const [documentos, setDocumentos] = useState([]);

    useEffect(() => {
        const obtenerDocumentos = async () => {
            try {
                const token = getToken();
                const response = await obtenerDocumento('documento.php?funcion=listar_documento_user&external=8c790f24-83e1-11ee-8e9c-5254008b9e28', token);
                const rpt = response.datos;
                console.log(response);
                //console.log("uno", resultado);
                setDocumentos(rpt);
               //console.log(documentos.datos);

            } catch (error) {
                console.error('Error', error);

            }
        };
        obtenerDocumentos();
    }, []);

    const AccionesMaterias = ({ }) => {
        return (
            <div style={{ display: 'flex', gap: '10px' }}>
                
                <a href="/noticias/nuevo" className="btn btn-outline-info btn-rounded">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-pencil-square" viewBox="0 0 16 16">
                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" />
                        <path fillRule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" />
                    </svg>
                </a>
                <a href="/noticias" className="btn btn-outline-danger btn-rounded">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-x-circle" viewBox="0 0 16 16">
                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z" />
                        <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
                    </svg>
                </a>
            </div>
        );
    };

    return (
        <div className="row">
            <h1>DOCUMENTOS</h1>
            <div className="container-fluid">
                {/* Agrega tu formulario de búsqueda aquí */}
                <div className="input-group mb-3">
                    <input type="text" className="form-control" placeholder="Buscar documentos" aria-label="Buscar materias" aria-describedby="button-addon2" />
                    <button className="btn btn-outline-secondary" type="button" id="button-addon2">Buscar</button>
                </div>
            </div>
            {/* Resto de tu código para el formulario de búsqueda y botón de registro */}
            <div className="col-4">
                <Link href="/documentos/registrar" className="btn btn-success">Registrar</Link>
            </div>

            <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Numero</th>
                        <th>Titulo</th>
                        <th>Autor</th>
                        <th>Subtotal</th>
                        <th>Iva</th>
                        <th>Total</th>
                        <th>Descuento</th>
                        <th>Isbn</th>
                        <th>Paginas</th>
                        <th>ACCIONES</th>
                        {/* Agregar más columnas según los datos obtenidos */}
                    </tr>
                </thead>
                <tbody>
                    {documentos.map((dato, index) => (
                        <tr key={index}>
                            <td>{index + 1}</td>
                            <td>{dato.titulo}</td>
                            <td>{dato.autor}</td>
                            <td>{dato.subtotal}</td>
                            <td>{dato.iva}</td>
                            <td>{dato.total}</td>
                            <td>{dato.descuento}</td>
                            <td>{dato.isbn}</td>
                            <td>{dato.paginas}</td>
                            <td>
                                <AccionesMaterias />
                            </td>
                            {/* Agregar más celdas según los datos obtenidos */}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}
