'use client';

//const MyContext = createContext();
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { inicio_sesion } from '@/hooks/Autenticacion';
import { estaSesion } from '@/hooks/SessionUtils';
import mensajes from '@/componentes/Mensajes';
//import { useRouter } from 'next/router';
import { useRouter } from 'next/navigation';
import Link from "next/link";
import { enviar } from '@/hooks/Conexion';

export default function Page() {

  const validationSchema = Yup.object().shape({
    titulo: Yup.string().required('Ingrese un titulo'),
    autor: Yup.string().required('Ingrese un autor')
  });
  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, formState } = useForm(formOptions);
  const { errors } = formState;

  const sendData = (data) => {
    var data = {
      "titulo": data.titulo,
      "autor": data.autor,
      "subtotal": data.subtotal,
      "iva": data.iva,
      "total": data.total,
      "descuento": data.descuento,
      "isbn": data.isbn,
      "paginas": data.paginas,
      "foto": data.foto,
      "user": data.user,
      "materia": data.materia,
      "funcion": "guardarDocumento"
    };
    enviar('documento.php', data, '8c790f24-83e1-11ee-8e9c-5254008b9e28')
      .then((info) => {
        console.log(info); // Aquí puedes hacer algo con la respuesta del servidor si es necesario
      })
      .catch((error) => {
        console.error('Error al enviar datos al servidor:', error);
      });




  };


  return (
    <div className="wrapper">
      <div className="d-flex flex-column">
        <div className="content">
          <h1>Registrar Documentos</h1>
          <div className='container-fluid'>
            <div className="col-lg-10">
              <div className="p-5">
                <form onSubmit={handleSubmit(sendData)}>

                  <div className="row">
                    <div className="col-md-6">
                      {/* Columna 1 */}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="titulo" className="form-label" style={{ fontWeight: 'bold' }}>Titulo</label>
                        <input {...register('titulo')} type="text" className="form-control form-control-user" placeholder="Ingrese el Titulo" />
                      </div>

                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="autor" className="form-label" style={{ fontWeight: 'bold' }}>Autor</label>
                        <input {...register('autor')} type="text" className="form-control form-control-user" placeholder="Ingrese el Autor" />
                      </div>
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="subtotal" className="form-label" style={{ fontWeight: 'bold' }}>Subtotal</label>
                        <input {...register('subtotal')} type="number" className="form-control form-control-user" placeholder="Ingrese el  subtotal" />
                      </div>

                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="iva" className="form-label" style={{ fontWeight: 'bold' }}>Iva</label>
                        <input {...register('iva')} type="number" className="form-control form-control-user" placeholder="Ingrese el  iva" />
                      </div>

                      {/* Continúa con el resto de los campos de la columna 1 */}
                    </div>

                    <div className="col-md-6">
                      {/* Columna 2 */}
                      {/** INGRESAR iva*/}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="total" className="form-label" style={{ fontWeight: 'bold' }}>Total</label>
                        <input {...register('total')} type="number" className="form-control form-control-user" placeholder="Ingrese el total" />
                      </div>
                      {/** INGRESAR iva*/}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="descuento" className="form-label" style={{ fontWeight: 'bold' }}>Descuento</label>
                        <input {...register('descuento')} type="number" className="form-control form-control-user" placeholder="Ingrese el descuento" />
                      </div>
                      {/** INGRESAR email */}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="isbn" className="form-label" style={{ fontWeight: 'bold' }}>Isbn</label>
                        <input {...register('isbn')} type="text" className="form-control form-control-user" placeholder="Ingrese el isbn" />
                      </div>
                      {/** INGRESAR iva*/}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="paginas" className="form-label" style={{ fontWeight: 'bold' }}>Paginas</label>
                        <input {...register('paginas')} type="number" className="form-control form-control-user" placeholder="Ingrese las paginas" />
                      </div>
                      {/** INGRESAR iva*/}
                      <div className="form-group" style={{ border: '1px solid #ced4da', borderRadius: '5px', padding: '8px' }}>
                        <label htmlFor="foto" className="form-label" style={{ fontWeight: 'bold' }}>Foto</label>
                        <input {...register('foto')} type="text" className="form-control form-control-user" placeholder="foto" />
                      </div>

                      {/* Continúa con el resto de los campos de la columna 2 */}
                    </div>
                  </div>

                  <hr />

                  {/** BOTÓN CANCELAR */}
                  <div style={{ display: 'flex', gap: '20px' }}>

                    <div className="d-grid gap-1">
                      <button type="submit" className="btn btn-primary btn-block mb-4">
                        <strong>Guardar</strong>
                      </button>
                    </div>
                    <div className="d-grid gap-1">
                      <button type="submit" className="btn btn-primary btn-block mb-4">
                        <Link href="/documentos" className="btn btn-danger btn-rounded">Cancelar</Link>

                      </button>

                    </div>

                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}