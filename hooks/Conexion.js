let URL = "https://computacion.unl.edu.ec/pdml/practica1/";
export function url_api() {
    return URL;
}

export async function obtener(recurso) {
    const response = await fetch(URL + recurso);
    return await response.json();
}



export async function enviar(recurso, data, key = '') {
    let headers = []
    if (key != '') {
        headers = {
            "Accept": "application/json",
            "TOKEN-KEY": key
        };
    } else {
        headers = {
            "Accept": "application/json",
        };

    }
    console.log(data);
    const response = await fetch(URL + recurso, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    });
    return await response.json();
}


export async function obtenerDocumento(recurso, key = '') {
    let headers = []
    if (key != '') {
        headers = {
            "Accept": "application/json",
            "TOKEN-KEY": key
        };
    } 
    const response = await fetch(URL + recurso, {
        method: "GET",
        headers: headers
    });
    return await response.json();
}

export async function enviarD(recurso, data, key = '') {
    const headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
    };

    if (key !== '') {
        headers["TOKEN-KEY"] = key;
    }

    try {
        const response = await fetch(URL + recurso, {
            method: "POST",
            headers: headers,
            body: JSON.stringify(data)
        });

        const responseData = await response.json();
        return responseData;
    } catch (error) {
        console.error('Error al enviar los datos:', error);
        throw error;
    }
}


