import { enviar } from "./Conexion";
import{save, saveToken} from "./SessionUtils"

export async function inicio_sesion(data){
    const sesion = await enviar('documento.php', data,"");

    if(sesion.code == 200 && sesion.jwt){
        saveToken(sesion.jwt);
        save('id',sesion.external);
        save('user',sesion.usuario);

    }
    return sesion;
}

//admina@admin.com
//admin